#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use Test::More;
use Capture::Tiny ':all';


# No output returns critical
{
    my ($stdout, $stderr, $exit_code) = capture {
        system("$Bin/../check_wireguard", "-b", "$Bin/wg_show_output/no_output", "-i", 1);
    };
    is(
        $exit_code / 256,
        2,
        "No interfaces return critical exit code"
    );
    is(
        $stdout,
        "WIREGUARD CRITICAL - Interfaces: Online:0 Expected:1 - Peers:\n",
        'No interfaces return CRITICAL text response'
    );
}

# Got one interfaces, expect one interfaces returns OK
{
    my ($stdout, $stderr, $exit_code) = capture {
        system("$Bin/../check_wireguard", "-b", "$Bin/wg_show_output/one_interfaces", "-i", 1);
    };
    is(
        $exit_code,
        0,
        "Got one interfaces, expect one interfaces returns zero exit code"
    );
    is(
        $stdout,
        "WIREGUARD OK - Interfaces: Online:1 Expected:1 - Peers: wg0:1/1\n",
        "Got one interfaces, expect one interfaces returns OK text response"
    );
}

# Got one interfaces, expect two interfaces returns CRITICAL
{
    my ($stdout, $stderr, $exit_code) = capture {
        system("$Bin/../check_wireguard", "-b", "$Bin/wg_show_output/one_interfaces", "-i", 2);
    };
    is(
        $exit_code / 256,
        2,
        "Got one interfaces, expect two interfaces return critical exit code"
    );
    is(
        $stdout,
        "WIREGUARD CRITICAL - Interfaces: Online:1 Expected:2 - Peers: wg0:1/1\n",
        'Got one interfaces, expect two interfaces return CRITICAL text response'
    );
}

# Got two interfaces, expect one interfaces returns CRITICAL
{
    my ($stdout, $stderr, $exit_code) = capture {
        system("$Bin/../check_wireguard", "-b", "$Bin/wg_show_output/two_interfaces", "-i", 1);
    };
    is(
        $exit_code / 256,
        2,
        "Got two interfaces, expect one interfaces return critical exit code"
    );
    is(
        $stdout,
        "WIREGUARD CRITICAL - Interfaces: Online:2 Expected:1 - Peers: wg0:1/1 wg1:1/1\n",
        'Got two interfaces, expect one interfaces return CRITICAL text response'
    );
}

done_testing();
