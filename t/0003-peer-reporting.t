#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use Test::More;
use Capture::Tiny ':all';


my $test_batches = [
    # seconds_connected_value, number of peers expected in result
    [ 3,        1 ],
    [ 30,       2 ],
    [ 80,       3 ],
    [ 19000,    4 ],
    [ 1382400,  5 ],
    [ 84672000, 6 ],
];

# seconds_connected set to 3 returns 2 peers
foreach my $test_batch (@$test_batches) {
    my $seconds_connected = $$test_batch[0];
    my $expected_peers = $$test_batch[1];
    my ($stdout, $stderr, $exit_code) = capture {
        system(
            "$Bin/../check_wireguard",
            "-b", "$Bin/wg_show_output/one_interfaces_eight_peers",
            "-i", 1,
            "-s", $seconds_connected
        );
    };
    is(
        $exit_code,
        0,
        "seconds_connected set to $seconds_connected return zero exit code"
    );
    is(
        $stdout,
        "WIREGUARD OK - Interfaces: Online:1 Expected:1 - Peers: wg0:$expected_peers/8\n",
        "seconds_connected set to $seconds_connected returns OK text response with $expected_peers peers connected"
    );
}

done_testing();
