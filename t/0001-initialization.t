#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use Test::More;
use Capture::Tiny ':all';


# Non executable binary returns critical
{
    my $non_executable_binary = "$Bin/wg_show_output/non_executable_wg";
    my ($stdout, $stderr, $exit_code) = capture {
        system("$Bin/../check_wireguard", "-b", $non_executable_binary);
    };
    is(
        $exit_code / 256,
        2,
        "Non executable binary returns critical exit code"
    );
    is(
        $stdout,
        "WIREGUARD CRITICAL - Path $non_executable_binary is not executable\n",
        'Non executable binary returns CRITICAL text response'
    );
}

# Non existant binary returns critical
{
    my ($stdout, $stderr, $exit_code) = capture {
        system("$Bin/../check_wireguard", "-b", "/this/wg/binary/does/not/exist");
    };
    is(
        $exit_code / 256,
        2,
        "Non existant binary returns critical exit code"
    );
    is(
        $stdout,
        "WIREGUARD CRITICAL - Path /this/wg/binary/does/not/exist is not executable\n",
        'Non existant binary returns CRITICAL text response'
    );
}

done_testing();
