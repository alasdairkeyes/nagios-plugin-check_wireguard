#!/bin/bash -e

prove t/*.t
for TFILE in t/*.t; do perl -MDevel::Cover $TFILE; done
cover +ignore_re ^t/.*t$ +ignore_re ^/var/lib/spamassassin +ignore_re Test/CountryFilter/Helper.pm
