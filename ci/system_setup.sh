#!/bin/bash

apt update

apt upgrade -y

# Install requirements for the code
apt install -y \
    libmonitoring-plugin-perl

# Install requirements for testing
apt install -y \
    libdevel-cover-perl \
    libcapture-tiny-perl \
    libtest-simple-perl \
    make \
    libpod-coverage-perl \
    perltidy

